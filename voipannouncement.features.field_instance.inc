<?php
/**
 * @file
 * voipannouncement.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function voipannouncement_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-blast-field_blast_audio'
  $field_instances['node-blast-field_blast_audio'] = array(
    'bundle' => 'blast',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'audiofield',
        'settings' => array(),
        'type' => 'audiofield_embedded',
        'weight' => 2,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_blast_audio',
    'label' => 'Audio message',
    'required' => 0,
    'settings' => array(
      'description_field' => 0,
      'file_directory' => '',
      'file_extensions' => 'wav mp3',
      'max_filesize' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'audiorecorderfield',
      'settings' => array(
        'progress_indicator' => 'throbber',
      ),
      'type' => 'audiorecorderfield_widget',
      'weight' => -2,
    ),
  );

  // Exported field_instance: 'node-blast-field_blast_phone_numbers'
  $field_instances['node-blast-field_blast_phone_numbers'] = array(
    'bundle' => 'blast',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Add new '.l('phone number', 'node/add/phone-number',
        array('query' => array('destination' => 'node/add/blast'),
          'attributes' => array('target'=>'_blank'))),
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'entityreference',
        'settings' => array(
          'link' => FALSE,
        ),
        'type' => 'entityreference_label',
        'weight' => 3,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_blast_phone_numbers',
    'label' => 'Phone Numbers',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => -1,
    ),
  );

  // Exported field_instance: 'node-blast-field_blast_text'
  $field_instances['node-blast-field_blast_text'] = array(
    'bundle' => 'blast',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_blast_text',
    'label' => 'Text message',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => -3,
    ),
  );

  // Exported field_instance: 'node-blast-field_blast_type'
  $field_instances['node-blast-field_blast_type'] = array(
    'bundle' => 'blast',
    'default_value' => array(
      0 => array(
        'value' => 'sms',
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_blast_type',
    'label' => 'Type',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => -4,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Audio message');
  t('Phone Numbers');
  t('Text message');
  t('Type');

  return $field_instances;
}
