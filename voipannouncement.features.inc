<?php
/**
 * @file
 * voip_announcement.features.inc
 */

/**
 * Implements hook_views_api().
 */
function voipannouncement_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function voipannouncement_node_info() {
  $items = array(
    'blast' => array(
      'name' => t('Announcement'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
