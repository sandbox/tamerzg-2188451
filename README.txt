== Introduction ==

The voipannouncement.module is a module which lets you send VoIP Announcements via SMS (Voice announcements coming soon) to a list of phone numbers.
Announcements will be queued using VoIP Blast module, and via dashboard you are able to Start/Pause/Stop any announcement as well to see status of each announcement.
For each announcement you are able to dive into detailed view where you can see VoIP Call status for each phone number in the queue.

== Dependencies ==
1. VoIP Drupal
2. VoIP Number
3. VoIP Blast
4. Features
5. Views
6. Views PHP
7. Views Data Export
8. Views Bulk Operations
9. Entity Reference
10. AudioRecorderField

== Installation ==

1. Extract voipannouncement.module to your sites/all/modules directory

2. Enable the VoIP Announcement module in admin/build/modules

3. Make sure you have configured VoIP Drupal (https://drupal.org/project/voipdrupal)
with at least one VoIP Provider in order to be able to send SMS messages.

== Using VoIP Announcement ==

1. Go to Add new content -> Add Announcement

2. Enter text or record audio message.
In case of audio message, URL link will be sent.

3. Select one or more phone numbers that will receive this Announcement.

4. Click Start

5. You will be redirected to Dashboard, where you can see status of the Announcement. For each announcement you are able to Start/Pause/Stop it.

6. Beside each Announcement you can click on Phone numbers row to see details about each phone number in Announcement.

---
The VoIP Announcement module has been originally developed by Tamer Zoubi
under the sponsorship of Terravoz (http://terravoz.org).

